<?php

/**
 * Utility to render views quick and easy
 *
 * @category    Utilities
 * @author      Bruno Foggia
 * @link        https://bitbucket.org/brunnofoggia/chillrender
 */
trait ChillRender {

    use \DarkTrait;

    /**
     * Renders request response to the output buffer
     * @reference noodlehaus/dispatch
     * @return string html
     */
    public function render($body, $code = 200, $headers = []) {
        http_response_code($code);
        array_walk($headers, function ($value, $key) {
            if (!preg_match('/^[a-zA-Z0-9\'`#$%&*+.^_|~!-]+$/', $key)) {
                throw new \InvalidArgumentException("Invalid header name - {$key}");
            }
            $values = is_array($value) ? $value : [$value];
            foreach ($values as $val) {
                if (
                        preg_match("#(?:(?:(?<!\r)\n)|(?:\r(?!\n))|(?:\r\n(?![ \t])))#", $val) ||
                        preg_match('/[^\x09\x0a\x0d\x20-\x7E\x80-\xFE]/', $val)
                ) {
                    throw new \InvalidArgumentException("Invalid header value - {$val}");
                }
            }
            header($key . ': ' . implode(',', $values));
        });
        echo $body;
    }

    /**
     * Render view files without layout
     * @param string $view path
     * @param array $vars variables sent for view context
     * @access protected
     */
    protected function renderPartialView($view, $vars = []) {
        $path = $this->getViewPath($view);
        if ($path !== false) {
            extract($vars, EXTR_SKIP);

            ob_start();
            require($path);
            $contents = ob_get_contents();
            @ob_end_clean();

            return $contents;
        }
        return false;
    }

    /**
     * Locate view path
     * @param string $view
     * @return string path
     */
    public function getViewPath($view) {
        $path = $view;
        !preg_match('/\.\w{1,4}$/', $path) && ($path .= '.php');
        ($viewPath = $this->getAttr('viewPath')) && (substr($viewPath, strlen($viewPath) - 1) !== '/') && ($viewPath .= '/');
        !is_file($path) && is_file($viewPath . $view) && ($path = $viewPath . $path);

        return is_file($path) ? $path : false;
    }

    /**
     * Render view files
     * @param view path
     * @param data variables sent for view context
     * @param layout allows to disable/enable layout or change it by sending its path
     * @access protected
     */
    protected function renderView($view, $data = [], $layout = true) {
        $content = $this->renderPartialView($view, $data, true);
        if ($content !== false) {
            if (!empty($layout) && !$this->isAjaxRequest()) {
                if (!is_string($layout)) {
                    $layout = $this->getAttr('layout');
                }

                $content = $this->renderViewLayout(['content' => $content], $layout);
            }

            $this->render($content);
        }
        return false;
    }

    /**
     * Render layout view
     * @param array $data variables sent for view context
     * @param string $layout path
     * @access protected
     */
    protected function renderViewLayout($vars = [], $layout = null) {
        empty($layout) && ($layout = $this->getAttr('layout'));
        if ($layout) {
            return $this->renderPartialView($layout, $vars);
        }
        return false;
    }

    /**
     * Test to see if a request contains the HTTP_X_REQUESTED_WITH header
     * @return bool
     */
    protected function isAjaxRequest() {
        return (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest');
    }

}
