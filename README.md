# ChillRender

[![Minimum PHP Version](http://img.shields.io/badge/php-%3E%3D%205.4-8892BF.svg)](https://php.net/)
[![License](https://img.shields.io/badge/license-MIT-4C9182.svg)](https://opensource.org/licenses/MIT)

Utility to render views quick and easy

## usage

1. Implement it into your class
 
        class Test {
            use \ChillRender;
        }

2. Set your paths

        protected $attrDefaults = [
            'layout' => 'views/layout', // file extension is .php by default, but you can send yours
            'viewPath' => 'views/'
        ];

3. Rendering

        // Just rendering
        $this->render('index');
        // Sending data to view
        $this->render('views/index', ['mydata' => $data]);
        // Rendering with other layout
        $this->render('views/index', [], 'views/other_layout');
        // Rendering without layout
        $this->render('views/index', [], false);
        // Rendering layout with custom content
        $this->renderLayout(['content' => $custom]);
        // Rendering a partial view
        $this->renderPartial('views/sidebar', ['mydata' => $data]);
        
  * Layout will not be rendered for ajax requests